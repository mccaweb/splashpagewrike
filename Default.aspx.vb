﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Newtonsoft.Json
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop

Partial Class _Default
    Inherits Page

    Dim dShowStartDate, dShowEndDate, dStartDate, dEndDate, sEventName, sEventFacility, sSponsorName, sEventURL, sSSID, sSSIDPassword, sContact, sEmail As String
    Dim BCECFolderID As String = "IEAAZ5OKI4EZHNJN"
    Dim HynesFolderID As String = "IEAAZ5OKI4EZHNM4"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("stoken") = Nothing Then
            Dim ClientID As String = "Sz9CIEe1"
            Dim ClientSecret As String = "oWCrsZoaFIRkBxQf5HTHuh9MY9Aj6E69uySba9YseCL8dTwu8VqsvF3RB3qmqfQP"
            Dim code As String = Request.QueryString("code")
            String.Format("https://www.wrike.com/oauth2/token?client_id={0}&client_secret={1}&grant_type=authorization_code&code={2}", ClientID, ClientSecret, code)

            If Request("code") Is Nothing Then
                Response.Redirect(String.Format("https://www.wrike.com/oauth2/authorize?client_id={0}&response_type=code", ClientID))
            Else
                Dim url As String = String.Format("https://www.wrike.com/oauth2/token?client_id={0}&client_secret={1}&grant_type=authorization_code&code={2}", ClientID, ClientSecret, code)
                Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
                request__1.Headers.Add("Access-Control-Allow-Origin", "*")
                request__1.Method = "Post"
                Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                    Dim reader As New StreamReader(response__2.GetResponseStream())

                    Dim token = reader.ReadToEnd()

                    Dim strArr() As String
                    Dim strArr1() As String
                    strArr = token.Split(",")
                    strArr1 = strArr(0).Split(":")
                    token = strArr1(1)
                    token = token.Replace("""", "").Trim
                    Session("stoken") = token
                End Using
            End If
        Else
            'Dim myToken As String
            'Dim strArr() As String
            'Dim strArr1() As String
            'myToken = Session("stoken")
            'strArr = myToken.Split(",")
            'strArr1 = strArr(0).Split(":")
            'myToken = strArr1(1)
            'myToken = myToken.Replace("""", "").Trim
            'Session("stoken") = myToken
        End If
    End Sub
    Private Sub SaveEvent()
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SplashPages").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            If session("sLoadedFromLocalDB") = True Then
                sqlString = "Update SplashPageInfo set EventName= @EventName, StartDate=@StartDate, EndDate=@EndDate, ContentDueDate=@ContentDueDate, Facility=@Facility, SponsorsName=@SponsorsName, ReDirectURL=@ReDirectURL, SSID=@SSID, SSIDPassword=@SSIDPassword, PrimaryContactFirstName=@PrimaryContactFirstName, ContactFullName=@ContactFullName, ContactEmail=@ContactEmail where EventCode=@EventCode"
            Else
                sqlString = "insert into SplashPageInfo values (@EventCode, @EventName, @StartDate, @EndDate, @ContentDueDate, @Facility, @SponsorsName, @ReDirectURL, @SSID, @SSIDPassword, @PrimaryContactFirstName, @ContactFullName, @ContactEmail)"
            End If

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)
                XmyCommand.Parameters.AddWithValue("@EventName", txtEventName.Text)
                XmyCommand.Parameters.AddWithValue("@StartDate", rdpStartDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@EndDate", rdpEndDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@ContentDueDate", rdpContentDueDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@Facility", txtFacility.Text)
                XmyCommand.Parameters.AddWithValue("@SponsorsName", txtSponsorName.Text)
                XmyCommand.Parameters.AddWithValue("@ReDirectURL", txtEventURL.Text)
                XmyCommand.Parameters.AddWithValue("@SSID", txtSSID.Text)
                XmyCommand.Parameters.AddWithValue("@SSIDPassword", txtSSIDPassword.Text)
                XmyCommand.Parameters.AddWithValue("@PrimaryContactFirstName", txtContactFirstName.Text)
                XmyCommand.Parameters.AddWithValue("@ContactFullName", txtContact.Text)
                XmyCommand.Parameters.AddWithValue("@ContactEmail", txtEmail.Text)

                XmyCommand.ExecuteNonQuery()
            End Using
        End Using

    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs)
        Dim url, lclFacility, lclFacilityName As String

        SaveEvent()

        If Not Session("stoken") Is Nothing Then

            'Session("stoken")	"{"access_token":"pwKblGeMrurZmXa01nDsBv9JcKbruPA7lQMBjlvYNLgTLSL6IQbZOyWA9cGUJvrY-N-N","token_type":"bearer","host":"www.wrike.com","expires_in":3600,"refresh_token":"F4Jph5MCcPfymF2LqX2L2O4hgEKeRNzKLGuNhARZr0BKQ0LTw1cMAZE4YieAa3lm-A-N"}" {String}	Object
            'curl -g -X GET -H 'Authorization: bearer <access_token>' 'https://www.wrike.com/api/v3/accounts/IEAGIITR/folders'
            ' IEAAZ5OKI4FMMROW
            'url = "https://www.wrike.com/api/v3/folders/" & Session("FolderName") & "/tasks?title=new
            'https://www.wrike.com/api/v3/folders/IEAAZ5OKI4FMMROW/tasks?title=newtasktestbob&dates={'start':'2017-10-03','due':'2017-10-10'}

            GetEventInfo()

            ' Determine which folder to use
            If sEventFacility = 1 Then
                lclFacility = BCECFolderID
                lclFacilityName = "BCEC"
            Else
                lclFacility = HynesFolderID
                lclFacilityName = "Hynes"
            End If


            ' Create Main Task
            url = "https://www.wrike.com/api/v3/folders/" & lclFacility & "/tasks?title=" & dShowStartDate & " - " & sEventName & "(Wi-Fi)&dates={'start':'" & dStartDate & "','due':'" & dEndDate & "'}&description=A.      Package Type: Wireless Network Sponsorship Package<br />" & _
            "SSID - " & sSSID & "<br />" & _
            "Password -" & sSSIDPassword & "<br />" & _
            "Splash Page - TBD<br />" & _
            "Contact - " & sContact & "<br />" & _
            "Contact Email - " & sEmail & "<br />" & _
            "url - " & sEventURL & "<br />" & _
            "B.      Dates: " & dShowStartDate & " to " & dShowEndDate & " <br />" & _
            "C.      Event ID: " & txtEventCode.Text & "<br />" & _
            "D.Venue: " & lclFacilityName
            Try

                Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
                request__1.Headers.Add("Authorization: Bearer " & Session("stoken"))
                request__1.Method = "Post"
                Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                    Dim reader As New StreamReader(response__2.GetResponseStream())
                    Dim sMainTask As String

                    Dim sResult = reader.ReadToEnd()
                    TextBox1.Text = sResult
                    sMainTask = GetReturnedID(sResult)
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Go Live (" & sEventName & " WiFi)"))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Move to captive portal (" & sEventName & " WiFi)"))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Notify NS (" & sEventName & " WiFi)"))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Test Site Tested and Approved (" & sEventName & " WiFi)"))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Test Site Created (" & sEventName & " WiFi)"))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Content Due (" & sEventName & " WiFi) " & rdpContentDueDate.SelectedDate))
                    TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Intro Email (" & sEventName & " WiFi)"))
                End Using
                Dim tDate As Date
                tDate = dShowStartDate
                AddNSNotificationCalendarAppointment(tDate, "Notify NS " & sEventName)
                AddMoveToCaptivePortalCalendarApppointment(tDate, "Move to Captive Portal " & sEventName)
                tDate = dShowEndDate
                AddAnalyticsRequestCalendarAppointment(tDate, "Analytics for " & sEventName)
                tDate = rdpContentDueDate.SelectedDate
                AddContentDueCalendarAppointment(tDate, "Analytics for " & sEventName)

                SendWelcomeEmail()
            Catch ex As Exception
                ' Error with email.  Mark as error and safe error message
                Label1.Text = ex.Message.ToString
            End Try
        End If
    End Sub

    Private Sub SendWelcomeEmail()


        Try

            If txtEmail.Text.Length > 0 Then
                '(1) Create the MailMessage instance
                Dim mm As New MailMessage()

                ' set the 'To:' to a noReply email address
                mm.From = New MailAddress("rplatz@massconvention.com", "Robert Platz")

                Dim strMailingMode As String
                strMailingMode = System.Configuration.ConfigurationManager.AppSettings("SplashPageEmailing")

                If strMailingMode <> Nothing Then
                    If (strMailingMode.ToUpper = "DEBUG") Then
                        ' blind copy everyone onto the email
                        mm.To.Add(New MailAddress("rplatz@signatureboston.com", "Robert Platz"))
                    Else
                        ' blind copy everyone onto the email
                        mm.To.Add(New MailAddress(txtEmail.Text, txtContact.Text))
                        mm.To.Add(New MailAddress("rplatz@signatureboston.com", "Robert Platz"))
                        mm.Bcc.Add(New MailAddress("jflaherty@bostoncmc.com ", "Jessica Flaherty"))
                    End If
                End If


                '(2) Assign the MailMessage's properties
                mm.Subject = txtEventName.Text & " -  Wireless Network Sponsorship"
                If txtContactFirstName.Text.Length > 0 Then
                    mm.Body = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
                Else
                    mm.Body = "Hi,<br/><br/>"
                End If

                Dim tDate As Date
                tDate = rdpContentDueDate.SelectedDate

                mm.Body = mm.Body & "I look forward to working with you and your team to create an engaging wireless network splash page.<br/><br/>" & _
                        "I have provided a OneDrive link to the Wireless Network Sponsorship Content Creation Guidelines and the Splash Page Template.  <a href='https://signatureboston-my.sharepoint.com/personal/vvieira_signatureboston_com/_layouts/15/guestaccess.aspx?folderid=14b325440b19f49a082300e16a4c213d4&authkey=AfhW2X818E2OZTVlcTkdOiM'>https://signatureboston-my.sharepoint.com/personal/vvieira_signatureboston_com/_layouts/15/guestaccess.aspx?folderid=14b325440b19f49a082300e16a4c213d4&authkey=AfhW2X818E2OZTVlcTkdOiM</a> The guidelines and template are tools that will assist you with creating your splash page.<br/><br/>" & _
                        "Please let me know what SSID name and URL re-direct you would like to use for your splash page.  The SSID is the public name of a wireless network, e.g., " & txtEventName.Text & " 2017.  The URL re-direct is the website the attendee will be directed to after they click 'Continue' on the splash page.  It is usually the show's or sponsor's website.<br/><br/>" & _
                        "The content submission deadline is " & tDate.ToLongDateString & ".  Once the content is received, we will test it and let you know if any adjustments are necessary.<br/><br/>" & _
                        "Feel free to contact me if you have any questions.<br/><br/>" & _
                        "All the best,<br/>" & _
                        "Bob"

                mm.IsBodyHtml = True

                '(3) Create the SmtpClient object
                Dim smtp As New SmtpClient

                '(4) Send the MailMessage (will use the Web.config settings)
                smtp.Send(mm)

                'System.Threading.Thread.Sleep(2000)

            End If

        Catch ex As Exception
            ' Error with email.  Mark as error and safe error message
        End Try
    End Sub
    Private Function GetReturnedID(ByVal sJSon As String) As String
        sJSon = sJSon.Replace("[", "")
        sJSon = sJSon.Replace("]", "")
        GetReturnedID = ""

        Dim reader As New JsonTextReader(New StringReader(sJSon))
        While reader.Read()
            If reader.Value IsNot Nothing Then
                If reader.Value = "id" Then
                    reader.Read()
                    GetReturnedID = reader.Value
                    Exit While
                End If
            End If
        End While

        Return (GetReturnedID)

    End Function

    Private Sub TurnIntoSubTask(ByVal sMainTaskID As String, ByVal sSubTaskID As String)
        Dim url As String

        If Not Session("stoken") Is Nothing Then

            ' Create new sub task.  It will get associated with the main task later
            url = "https://www.wrike.com/api/v3/tasks/" & sSubTaskID & "?addSuperTasks=['" & sMainTaskID & "']"
            Try

                Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
                request__1.Headers.Add("Authorization: Bearer " & Session("stoken"))
                request__1.Method = "PUT"
                Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                    Dim reader As New StreamReader(response__2.GetResponseStream())

                    Dim sResult = reader.ReadToEnd()
                    TextBox1.Text = sResult
                End Using

            Catch ex As Exception
                ' Error with email.  Mark as error and safe error message
                Label1.Text = ex.Message.ToString
            End Try
        End If
    End Sub
    Private Function CreateSubTask(ByVal sMainTaskID As String, ByVal sTitle As String) As String
        Dim url As String
        CreateSubTask = "0"

        If Not Session("stoken") Is Nothing Then

            ' Create new sub task.  It will get associated with the main task later
            url = "https://www.wrike.com/api/v3/folders/" & sMainTaskID & "/tasks?title=" & sTitle
            Try

                Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
                request__1.Headers.Add("Authorization: Bearer " & Session("stoken"))
                request__1.Method = "Post"
                Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                    Dim reader As New StreamReader(response__2.GetResponseStream())

                    Dim sResult = reader.ReadToEnd()
                    TextBox1.Text = sResult
                    CreateSubTask = GetReturnedID(sResult)
                End Using

            Catch ex As Exception
                ' Error with email.  Mark as error and safe error message
                Label1.Text = ex.Message.ToString
            End Try
        End If

        Return CreateSubTask
    End Function

    Private Sub GetEventInfo()
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EMS-Production").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            ' remember to add the event code later
            sqlString = "SELECT CONVERT(char(10), start_date,126) as eventstart, CONVERT(char(10), end_date,126) as eventend,CONVERT(char(10), start_date,101) as showeventstart, CONVERT(char(10), end_date,101) as showeventend, name  AS name, facility_id AS facility from ems_event WHERE (ems_event.eventCode =  @EventCode)"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()
                        If IsNothing(rdpStartDate.SelectedDate) Then
                            dShowStartDate = XmyReader("showeventstart")
                            dStartDate = XmyReader("eventstart")
                        Else
                            dShowStartDate = rdpStartDate.SelectedDate
                            dStartDate = rdpStartDate.SelectedDate
                        End If
                        If IsNothing(rdpEndDate.SelectedDate) Then
                            dShowEndDate = XmyReader("showeventend")
                            dEndDate = XmyReader("eventend")
                        Else
                            dShowEndDate = rdpEndDate.SelectedDate
                            dEndDate = rdpEndDate.SelectedDate
                        End If

                        'Wrike requires date to be yyyy-mm-dd
                        Dim tDate As Date
                        tDate = dStartDate
                        dStartDate = tDate.ToString("yyyy-MM-dd")
                        tDate = dEndDate
                        dEndDate = tDate.ToString("yyyy-MM-dd")

                        If txtEventName.Text.Length > 0 Then
                            sEventName = txtEventName.Text
                        Else
                            sEventName = XmyReader("name")
                        End If
                        If txtFacility.Text.Length > 0 Then
                            sEventFacility = txtFacility.Text
                        Else
                            sEventFacility = XmyReader("facility")
                        End If
                    Loop
                End Using
            End Using
        End Using

        ' See if any of the loaded event parameters are being overwritten by the user
        If txtEventName.Text.Length > 0 Then
            sEventName = txtEventName.Text
        End If

        If txtSponsorName.Text.Length > 0 Then
            sSponsorName = txtSponsorName.Text
        End If
        If txtEventURL.Text.Length > 0 Then
            sEventURL = txtEventURL.Text
        Else
            sEventURL = "(TBD)"
        End If
        If txtSSID.Text.Length > 0 Then
            sSSID = txtSSID.Text & " (from Terms Sheet)"
        Else
            sSSID = "(TBD)"
        End If
        If txtSSIDPassword.Text.Length > 0 Then
            sSSIDPassword = txtSSIDPassword.Text & " (from Terms Sheet)"
        Else
            sSSIDPassword = "(TBD)"
        End If
        If txtContact.Text.Length > 0 Then
            sContact = txtContact.Text
        Else
            sContact = ""
        End If
        If txtEmail.Text.Length > 0 Then
            sEmail = txtEmail.Text
        Else
            sEmail = ""
        End If

        ' Make sure there isn't any special chars in the strings that can't be passed on the URL
        sEventName = Server.UrlEncode(sEventName)
        sSponsorName = Server.UrlEncode(sSponsorName)
        sEventURL = Server.UrlEncode(sEventURL)
        sSSID = Server.UrlEncode(sSSID)
        sSSIDPassword = Server.UrlEncode(sSSIDPassword)

    End Sub
    Private Sub LoadFromEMS()

        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EMS-Production").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            ' remember to add the event code later
            sqlString = "SELECT CONVERT(char(10), start_date,126) as eventstart, CONVERT(char(10), end_date,126) as eventend,CONVERT(char(10), start_date,101) as showeventstart, CONVERT(char(10), end_date,101) as showeventend, name  AS name, facility_id AS facility from ems_event WHERE (ems_event.eventCode =  @EventCode)"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()
                        rdpStartDate.DbSelectedDate = XmyReader("showeventstart")
                        rdpEndDate.DbSelectedDate = XmyReader("showeventend")
                        txtEventName.Text = XmyReader("name")
                        txtFacility.Text = XmyReader("facility")
                        'If txtFacility.Text = 1 Then
                        'txtFacility.Text = "BCEC"
                        'Else
                        'txtFacility.Text = "Hynes"
                        'End If
                    Loop
                End Using
            End Using
        End Using

        Dim tDate As Date
        tDate = rdpEndDate.SelectedDate
        rdpContentDueDate.SelectedDate = tDate.AddDays(-30)
        txtSponsorName.Text = "TBD"
        txtEventURL.Text = "TBD"
        txtSSID.Text = "TBD"
        txtSSIDPassword.Text = "TBD"
        txtContactFirstName.Text = "TBD"
        txtContact.Text = "TBD"
        txtEmail.Text = "TBD"

    End Sub

    Private Function LoadFromLocalDB()
        LoadFromLocalDB = False
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SplashPages").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            ' remember to add the event code later
            sqlString = "SELECT * from SplashPageInfo WHERE eventCode =  @EventCode"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()
                        LoadFromLocalDB = True
                        txtEventName.Text = XmyReader("EventName")
                        rdpStartDate.DbSelectedDate = XmyReader("StartDate")
                        rdpEndDate.DbSelectedDate = XmyReader("EndDate")
                        rdpContentDueDate.SelectedDate = XmyReader("ContentDueDate")
                        txtFacility.Text = XmyReader("facility")
                        txtSponsorName.Text = XmyReader("SponsorsName")
                        txtEventURL.Text = XmyReader("ReDirectURL")
                        txtSSID.Text = XmyReader("SSID")
                        txtSSIDPassword.Text = XmyReader("SSIDPassword")
                        txtContactFirstName.Text = XmyReader("PrimaryContactFirstName")
                        txtContact.Text = XmyReader("ContactFullName")
                        txtEmail.Text = XmyReader("ContactEmail")
                    Loop
                End Using
            End Using
        End Using

        Return LoadFromLocalDB
    End Function
    Protected Sub btnLoadEvent_Click(sender As Object, e As EventArgs)
        session("sLoadedFromLocalDB") = True

        If LoadFromLocalDB() = False Then
            session("sLoadedFromLocalDB") = False
            LoadFromEMS()
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs)
        SaveEvent()
    End Sub

    Protected Sub AddContentDueCalendarAppointment(ByVal sDueDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = "Content is due: " & sEventName & " on " & sDueDate
        TempAppItem.Body = "Hi " & txtContactFirstName.Text & ",<br/><br/>" & _
            "I hope all is well!  I wanted to check in to see if you had any questions regarding the creation of your splash page, and to give you a friendly reminder that the content submission deadline is " & sDueDate.ToLongDateString & "<br/><br/>" & _
            "Feel free to contact me if you have any questions.<br/><br/>" & _
            "All the best,<br/><br/>Bob"

        'Set Location
        TempAppItem.Location = "No Location"

        Dim sTempStartDate, sTempEndDate As String
        ' if Sunday add two days from endate
        If sDueDate.DayOfWeek = DayOfWeek.Sunday Then
            sTempStartDate = sDueDate.AddDays(-2).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sDueDate.AddDays(-2).ToShortDateString() & " 8:00:00 AM"
            ' if  Saturday add 3 days to enddate
        ElseIf sDueDate.DayOfWeek = DayOfWeek.Saturday Then
            sTempStartDate = sDueDate.AddDays(-1).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sDueDate.AddDays(-1).ToShortDateString() & " 8:00:00 AM"

        Else ' else just add one day
            sTempStartDate = sDueDate.ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sDueDate.ToShortDateString() & " 8:00:00 AM"

        End If

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub
    Protected Sub AddAnalyticsRequestCalendarAppointment(ByVal sEndDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = "Please send analytics for SSID: " & sEventName & " from " & dShowStartDate & " to " & dShowEndDate
        'Set Location
        TempAppItem.Location = "No Location"

        Dim sTempStartDate, sTempEndDate As String
        ' if Sunday add two days from endate
        If sEndDate.AddDays(1).DayOfWeek = DayOfWeek.Sunday Then
            sTempStartDate = sEndDate.AddDays(2).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sEndDate.AddDays(2).ToShortDateString() & " 8:00:00 AM"
            ' if  Saturday add 3 days to enddate
        ElseIf sEndDate.AddDays(1).DayOfWeek = DayOfWeek.Saturday Then
            sTempStartDate = sEndDate.AddDays(3).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sEndDate.AddDays(3).ToShortDateString() & " 8:00:00 AM"

        Else ' else just add one day
            sTempStartDate = sEndDate.AddDays(1).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sEndDate.AddDays(1).ToShortDateString() & " 8:00:00 AM"

        End If

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub

    Protected Sub AddMoveToCaptivePortalCalendarApppointment(ByVal sStartDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = sSubject
        'Set Location
        TempAppItem.Location = "No Location"

        Dim sTempStartDate, sTempEndDate As String
        ' if reminder to activated would land on a Sunday push back to Friday
        If sStartDate.AddDays(-1).DayOfWeek = DayOfWeek.Sunday Then
            sTempStartDate = sStartDate.AddDays(-3).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-3).ToShortDateString() & " 8:00:00 AM"
            ' if reminder to activated would land on a Saturday push back to Friday
        ElseIf sStartDate.AddDays(-1).DayOfWeek = DayOfWeek.Saturday Then
            sTempStartDate = sStartDate.AddDays(-2).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-2).ToShortDateString() & " 8:00:00 AM"

        Else ' else just back up one day
            sTempStartDate = sStartDate.AddDays(-1).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-1).ToShortDateString() & " 8:00:00 AM"

        End If

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub
    Protected Sub AddNSNotificationCalendarAppointment(ByVal sStartDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = sSubject
        'Set Location
        TempAppItem.Location = "No Location"

        Dim sTempStartDate, sTempEndDate As String
        ' if reminder to activated would land on a Sunday push back to Friday
        If sStartDate.AddDays(-1).DayOfWeek = DayOfWeek.Sunday Then
            sTempStartDate = sStartDate.AddDays(-3).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-3).ToShortDateString() & " 8:00:00 AM"
            ' if reminder to activated would land on a Saturday push back to Friday
        ElseIf sStartDate.AddDays(-1).DayOfWeek = DayOfWeek.Saturday Then
            sTempStartDate = sStartDate.AddDays(-2).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-2).ToShortDateString() & " 8:00:00 AM"

        Else ' else just back up one day
            sTempStartDate = sStartDate.AddDays(-1).ToShortDateString() & " 7:00:00 AM"
            sTempEndDate = sStartDate.AddDays(-1).ToShortDateString() & " 8:00:00 AM"

        End If

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub

    Protected Sub btnWelcomeEmail_Click(sender As Object, e As EventArgs)
        SendWelcomeEmail()
    End Sub
End Class
