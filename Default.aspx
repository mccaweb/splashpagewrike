﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <br /><br />
    <telerik:RadMenu ID="RadMenu1" runat="server">
    <Items>
    <telerik:RadMenuItem runat="server" Text="Wrike Entry">
    </telerik:RadMenuItem>
    <telerik:RadMenuItem runat="server" Text="EMailings">
        <Items>
            <telerik:RadMenuItem runat="server" Text="Welcome">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="Content Reminder">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="NS Notification">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem runat="server" Text="SSID &amp; URL Reminder">
            </telerik:RadMenuItem>
        </Items>
    </telerik:RadMenuItem>
    </Items>
    </telerik:RadMenu>
    <br /><br />

    <div class="form-group">
        
        <div class="col-sm-6 col-md-6 col-lg-6">
            <label class="control-label col-sm-2 col-md-2 col-lg-2">Event Code: </label>
            <telerik:RadTextBox ID="txtEventCode" runat="server" class="control-label col-sm-2 col-md-2 col-lg-2" Resize="None" Width="60px"></telerik:RadTextBox> <asp:Button ID="btnLoadEvent" runat="server" Text="load Event" OnClick="btnLoadEvent_Click"/>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Event Name:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtEventName" runat="server" LabelWidth="64px" Resize="None" Width="500px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Start Date:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadDatePicker ID="rdpStartDate" runat="server"></telerik:RadDatePicker>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">End Date:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadDatePicker ID="rdpEndDate" runat="server"></telerik:RadDatePicker>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Content Due:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadDatePicker ID="rdpContentDueDate" runat="server"></telerik:RadDatePicker>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Facility:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtFacility" runat="server" LabelWidth="64px" Resize="None" ReadOnly="true" Width="200px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Sponsors Name:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtSponsorName" runat="server" LabelWidth="64px" Resize="None" Width="200px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Re-direct URL:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtEventURL" runat="server" LabelWidth="64px" Resize="None" Width="300px"></telerik:RadTextBox>
        </div>
    </div>    
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">SSID:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtSSID" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">SSID Password:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtSSIDPassword" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Primary Contact First Name:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtContactFirstName" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Contact:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtContact" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2 col-md-2 col-lg-2">Contact Email:</label>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <telerik:RadTextBox ID="txtEmail" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
        </div>
    </div>
    <asp:Button ID="Button4" runat="server" Text="Create Wrike Entry" OnClick="Button3_Click"/><br /><br />
    <asp:Button ID="btnWelcomeEmail" runat="server" Text="Send Welcome EMail" OnClick="btnWelcomeEmail_Click"/><br /><br />
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
    <asp:TextBox ID="TextBox1" TextMode="MultiLine" Rows="10" runat="server"></asp:TextBox>
</asp:Content>

