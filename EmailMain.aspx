﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="EmailMain.aspx.vb" Inherits="EmailMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <link href="styles.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
             <div class="demo-container no-bg">
                <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" >
                    <Tabs>
                        <telerik:RadTab Text="Event" Width="200px"></telerik:RadTab>
                        <telerik:RadTab Text="Emailings" Width="200px"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0" CssClass="outerMultiPage">
                    <telerik:RadPageView runat="server" ID="RadPageView1">
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="" class="control-label col-sm-3 col-md-3 col-lg-3"></asp:Label><br />
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="" class="control-label col-sm-3 col-md-3 col-lg-3"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Event Code:</label>
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <telerik:RadTextBox ID="txtEventCode" runat="server" Resize="None" Width="60px"></telerik:RadTextBox>
                            </div >
                            <div >
                                <asp:Button ID="btnLoadEvent" runat="server" Text="Load Event" OnClick="btnLoadEvent_Click"/>
                                <asp:Button ID="btnSaveEvent" runat="server" Text="Save Event" OnClick="btnSaveEvent_Click"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Event Name:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtEventName" runat="server" LabelWidth="64px" Resize="None" Width="500px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Start Date:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadDatePicker ID="rdpStartDate" runat="server"></telerik:RadDatePicker>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">End Date:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadDatePicker ID="rdpEndDate" runat="server"></telerik:RadDatePicker>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Content Due:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadDatePicker ID="rdpContentDueDate" runat="server"></telerik:RadDatePicker>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Facility:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtFacility" runat="server" LabelWidth="64px" Resize="None" ReadOnly="true" Width="200px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Sponsors Name:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtSponsorName" runat="server" LabelWidth="64px" Resize="None" Width="200px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Re-direct URL:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtEventURL" runat="server" LabelWidth="64px" Resize="None" Width="300px"></telerik:RadTextBox>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">SSID:</label><asp:CheckBox ID="chkSSIDTermSheet" runat="server" Text=" (from Terms Sheet)" />
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtSSID" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">SSID Password:</label><asp:CheckBox ID="chkPasswordTermSheet" runat="server" Text=" (from Terms Sheet)" />
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtSSIDPassword" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Clear Case:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtClearCase" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Primary Contact First Name:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtContactFirstName" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Contact:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtContact" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-lg-3">Contact Email:</label>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <telerik:RadTextBox ID="txtEmail" runat="server" LabelWidth="64px" Resize="None" Width="160px"></telerik:RadTextBox>
                            </div>
                        </div>
                        <asp:Button ID="Button4" runat="server" Text="Create Wrike Entry" OnClick="Button3_Click"/><br /><br />
                        <asp:TextBox ID="TextBox1" TextMode="MultiLine" Rows="10" runat="server"></asp:TextBox>
                    </telerik:RadPageView>
                    <telerik:RadPageView runat="server" ID="RadPageView2">
                        <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip2"  MultiPageID="RadMultiPage2"
                            Orientation="VerticalLeft" Width="150px" Height="355px" SelectedIndex="0">
                            <Tabs>
                                <telerik:RadTab  Height="40px" Text="Welcome"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="Content Received"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="Test Site Ready"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="Content Reminder"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="NS Notification"></telerik:RadTab>
                                <telerik:RadTab  Height="60px" Text="SSID & URL Reminder"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="Request Analytics"></telerik:RadTab>
                                <telerik:RadTab  Height="40px" Text="Test Email"></telerik:RadTab>
                           </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage runat="server" ID="RadMultiPage2"  SelectedIndex="0" CssClass="innerMultiPage">
                            <telerik:RadPageView runat="server" ID="PageView10">
                                <div class="recipeImage qsf-ib">
                                    <asp:Button ID="btnSendWelcome" runat="server" Text="Send Email" OnClick="btnSendWelcome_Click" />
                                </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtWelcome" runat="server" Width="400px" EditModes="All">
                                    </telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView runat="server" ID="PageView20">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnSendContentReceived" runat="server" Text="Send Email" OnClick="btnSendContentReceived_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtContentReceived" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                             <telerik:RadPageView runat="server" ID="PageView30">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnSendTestSite" runat="server" Text="Send Email" OnClick="btnSendTestSite_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtTestSite" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                           <telerik:RadPageView runat="server" ID="PageView40">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnSendContentReminder" runat="server" Text="Send Email" OnClick="btnSendContentReminder_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtContentReminder" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView runat="server" ID="PageView50">
                                <div class="recipeImage qsf-ib">
                                      <asp:Button ID="btnSendNSNotification" runat="server" Text="Send Email" OnClick="btnSendNSNotification_Click" />
                              </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtNSNotification" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView runat="server" ID="PageView60">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnSendSSIDReminder" runat="server" Text="Send Email" OnClick="btnSendSSIDReminder_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtSSIDReminder" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView runat="server" ID="RadPageView70">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnSendAnalyticsRequest" runat="server" Text="Send Email" OnClick="btnSendAnalyticsRequest_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtSendAnalyticsRequest" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView runat="server" ID="RadPageView80">
                                <div class="recipeImage qsf-ib">
                                     <asp:Button ID="btnTestEmail" runat="server" Text="Send Email" OnClick="btnSendTest_Click" />
                               </div>
                                <div class="ingredients qsf-ib">
                                    <telerik:RadEditor ID="txtTestEmail" runat="server" Width="400px" EditModes="All"></telerik:RadEditor>
                                </div>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>

</asp:Content>

