﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Newtonsoft.Json
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop

Partial Class EmailMain
    Inherits System.Web.UI.Page

    Dim dShowStartDate, dShowEndDate, dStartDate, dEndDate As String
    Dim BCECFolderID As String = "IEAAZ5OKI4JFCZPE" ' 2019 BCEC WiFi
    Dim HynesFolderID As String = "IEAAZ5OKI4JFCZV5" ' 2019 Hynes WiFi
    Dim PrimanentToken As String = "eyJ0dCI6InAiLCJhbGciOiJIUzI1NiIsInR2IjoiMSJ9.eyJkIjoie1wiYVwiOjg0OTM1NCxcImlcIjo1NzcwOTg3LFwiY1wiOjM3NjY0OTAsXCJ2XCI6bnVsbCxcInVcIjozMjIzNzAzLFwiclwiOlwiVVNcIixcInNcIjpbXCJXXCIsXCJGXCIsXCJJXCIsXCJVXCIsXCJLXCIsXCJDXCJdLFwielwiOltdLFwidFwiOjB9IiwiaWF0IjoxNTQ3MjEwOTI3fQ.UFfYMNKGHbwKyrcqRnLilnJKPLj9vZhTtSucoTTF0Uo"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Page.IsPostBack Then
            If Not rdpStartDate.SelectedDate Is Nothing Then
                dShowStartDate = rdpStartDate.SelectedDate
                dShowEndDate = rdpEndDate.SelectedDate
                dStartDate = rdpStartDate.SelectedDate
                dEndDate = rdpEndDate.SelectedDate
            End If
        End If
        'Session("stoken") = "eyJ0dCI6InAiLCJhbGciOiJIUzI1NiIsInR2IjoiMSJ9.eyJkIjoie1wiYVwiOjg0OTM1NCxcImlcIjo1NzYzOTE3LFwiY1wiOjM3NjY0OTAsXCJ2XCI6bnVsbCxcInVcIjozMjIzNzAzLFwiclwiOlwiVVNcIixcInNcIjpbXCJXXCIsXCJGXCIsXCJJXCIsXCJVXCIsXCJLXCIsXCJDXCJdLFwielwiOltdLFwidFwiOjB9IiwiaWF0IjoxNTQ3MDU4NDk1fQ.1NqwRHutnihzpDcNb7TFEyVp8X58Hf5TlJ0vxGLOiTc.eyJkIjoie1wiYVwiOjg0OTM1NCxcImlcIjo1NzYzNDY2LFwiY1wiOjM3NjY0OTAsXCJ2XCI6bnVsbCxcInVcIjozMjIzNzAzLFwiclwiOlwiVVNcIixcInNcIjpbXCJXXCIsXCJGXCIsXCJJXCIsXCJVXCIsXCJLXCIsXCJDXCJdLFwielwiOltdLFwidFwiOjB9IiwiaWF0IjoxNTQ3MDUyMDY5fQ.Isc6dKwFo5QQM_x1-tzSBWg7KOwQX4dYtIykyNhFk_c"

        ' commented out because I'm using PrimanentToken
        'If Session("stoken") = Nothing Then
        'Dim ClientID As String = "Sz9CIEe1"
        'Dim ClientSecret As String = "oWCrsZoaFIRkBxQf5HTHuh9MY9Aj6E69uySba9YseCL8dTwu8VqsvF3RB3qmqfQP"
        'Dim code As String = Request.QueryString("code")
        'String.Format("https://www.wrike.com/oauth2/token?client_id={0}&client_secret={1}&grant_type=authorization_code&code={2}", ClientID, ClientSecret, code)

        'Try

        'If Request("code") Is Nothing Then
        'GetRequestCode(ClientID)
        'Else
        'PostRequest(ClientID, ClientSecret, code)
        'End If
        'Catch ex As Exception
        'GetRequestCode(ClientID)
        'PostRequest(ClientID, ClientSecret, code)
        'End Try
        'End If
        ' end of PrimanentToken
    End Sub
    Private Sub GetRequestCode(ByVal ClientID As String)
        Response.Redirect(String.Format("https://www.wrike.com/oauth2/authorize/v3?client_id={0}&response_type=code", ClientID))
    End Sub
    Private Sub PostRequest(ByVal ClientID As String, ByVal ClientSecret As String, ByVal code As String)
        Try
            Dim url As String = String.Format("https://www.wrike.com/oauth2/token?client_id={0}&client_secret={1}&grant_type=authorization_code&code={2}", ClientID, ClientSecret, code)
            Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
            request__1.Headers.Add("Access-Control-Allow-Origin", "*")
            request__1.Method = "Post"
            Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                Dim reader As New StreamReader(response__2.GetResponseStream())

                Dim token = reader.ReadToEnd()

                Dim strArr() As String
                Dim strArr1() As String
                strArr = token.Split(",")
                strArr1 = strArr(0).Split(":")
                token = strArr1(1)
                token = token.Replace("""", "").Trim
                Session("stoken") = token
            End Using
        Catch ex As Exception
            Dim mystring As String
            mystring = ex.Message.ToString
        End Try

    End Sub
    Private Sub SaveEvent()
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SplashPages").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            If Session("sLoadedFromLocalDB") = True Then
                sqlString = "Update SplashPageInfo set EventName= @EventName, StartDate=@StartDate, EndDate=@EndDate, ContentDueDate=@ContentDueDate, Facility=@Facility, SponsorsName=@SponsorsName, ReDirectURL=@ReDirectURL, SSID=@SSID, SSIDPassword=@SSIDPassword, PrimaryContactFirstName=@PrimaryContactFirstName, ContactFullName=@ContactFullName, ContactEmail=@ContactEmail, ssidtermsheet=@ssidtermsheet, passwordtermsheet=@passwordtermsheet, ClearCase=@ClearCase where EventCode=@EventCode"
            Else
                sqlString = "insert into SplashPageInfo values (@EventCode, @EventName, @StartDate, @EndDate, @ContentDueDate, @Facility, @SponsorsName, @ReDirectURL, @SSID, @SSIDPassword, @PrimaryContactFirstName, @ContactFullName, @ContactEmail, @ssidtermsheet, @passwordtermsheet, @ClearCase)"
            End If


            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)
                XmyCommand.Parameters.AddWithValue("@EventName", txtEventName.Text)
                XmyCommand.Parameters.AddWithValue("@StartDate", rdpStartDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@EndDate", rdpEndDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@ContentDueDate", rdpContentDueDate.SelectedDate)
                XmyCommand.Parameters.AddWithValue("@Facility", txtFacility.Text)
                XmyCommand.Parameters.AddWithValue("@SponsorsName", txtSponsorName.Text)
                XmyCommand.Parameters.AddWithValue("@ReDirectURL", txtEventURL.Text)
                XmyCommand.Parameters.AddWithValue("@SSID", txtSSID.Text)
                XmyCommand.Parameters.AddWithValue("@SSIDPassword", txtSSIDPassword.Text)
                XmyCommand.Parameters.AddWithValue("@ClearCase", txtClearCase.Text)
                XmyCommand.Parameters.AddWithValue("@PrimaryContactFirstName", txtContactFirstName.Text)
                XmyCommand.Parameters.AddWithValue("@ContactFullName", txtContact.Text)
                XmyCommand.Parameters.AddWithValue("@ContactEmail", txtEmail.Text)
                If chkSSIDTermSheet.Checked = True Then
                    XmyCommand.Parameters.AddWithValue("@ssidtermsheet", 1)
                Else
                    XmyCommand.Parameters.AddWithValue("@ssidtermsheet", 0)
                End If
                If chkPasswordTermSheet.Checked = True Then
                    XmyCommand.Parameters.AddWithValue("@passwordtermsheet", 1)
                Else
                    XmyCommand.Parameters.AddWithValue("@passwordtermsheet", 0)
                End If

                XmyCommand.ExecuteNonQuery()
            End Using
        End Using

    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs)
        Dim url, lclFacility, lclFacilityName As String

        SaveEvent()

        ' commenting out if statement because I'm using a PrimanentToken
        'If Not Session("stoken") Is Nothing Then

        'Session("stoken")	"{"access_token":"pwKblGeMrurZmXa01nDsBv9JcKbruPA7lQMBjlvYNLgTLSL6IQbZOyWA9cGUJvrY-N-N","token_type":"bearer","host":"www.wrike.com","expires_in":3600,"refresh_token":"F4Jph5MCcPfymF2LqX2L2O4hgEKeRNzKLGuNhARZr0BKQ0LTw1cMAZE4YieAa3lm-A-N"}" {String}	Object
        'curl -g -X GET -H 'Authorization: bearer <access_token>' 'https://www.wrike.com/api/v3/accounts/IEAAZ5OKI4EZHNJN/folders'
        ' IEAAZ5OKI4FMMROW
        'url = "https://www.wrike.com/api/v3/folders/" & Session("FolderName") & "/tasks?title=new
        'https://www.wrike.com/api/v3/folders/IEAAZ5OKI4FMMROW/tasks?title=newtasktestbob&dates={'start':'2017-10-03','due':'2017-10-10'}

        'GetEventInfo()

        ' Determine which folder to use
        If txtFacility.Text = 1 Then
            lclFacility = BCECFolderID
            lclFacilityName = "BCEC"
        Else
            lclFacility = HynesFolderID
            lclFacilityName = "Hynes"
        End If

        If chkSSIDTermSheet.Checked = True Then
            txtSSID.Text = txtSSID.Text & " (from Terms Sheet)"
        End If
        If chkPasswordTermSheet.Checked = True Then
            txtSSIDPassword.Text = txtSSIDPassword.Text & " (from Terms Sheet)"
        End If

        Dim tEndDate, tStartDate As Date

        tEndDate = dEndDate
        tStartDate = dStartDate
        ' Create Main Task
        url = "https://www.wrike.com/api/v4/folders/" & lclFacility & "/tasks?title=" & dShowStartDate & " - " & txtEventName.Text & "(Wi-Fi)&dates={'start':'" & tStartDate.ToString("yyyy-MM-dd") & "','due':'" & tEndDate.ToString("yyyy-MM-dd") & "'}&description=A.      Package Type: Wireless Network Sponsorship Package<br />" &
           "SSID - " & txtSSID.Text & "<br />" &
          "Password -" & txtSSIDPassword.Text & "<br />" &
         "Splash Page - Yes<br />" &
            "Contact - " & txtContact.Text & "<br />" &
           "Contact Email - " & txtEmail.Text & "<br />" &
          "url - " & txtEventURL.Text & "<br />" &
         "B.      Dates: " & dShowStartDate & " to " & dShowEndDate & " <br />" &
            "C.      Event ID: " & txtEventCode.Text & "<br />" &
           "D.Venue: " & lclFacilityName & "<br /><br />" &
          "ClearCase: " & txtClearCase.Text
        Try

            Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
            request__1.Method = "Post"
            request__1.KeepAlive = False
            request__1.ProtocolVersion = HttpVersion.Version10
            request__1.ServicePoint.ConnectionLimit = 1
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            request__1.Headers("Authorization") = "Bearer " & PrimanentToken
            Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                Dim reader As New StreamReader(response__2.GetResponseStream())
                Dim sMainTask As String

                Dim sResult = reader.ReadToEnd()
                TextBox1.Text = sResult
                sMainTask = GetReturnedID(sResult)
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Go Live (" & txtEventName.Text & " WiFi)", rdpStartDate.SelectedDate))

                Dim dAssignStartDate As Date
                Dim dDummay As Date = "1/1/1900"

                ' Move code to captive portal 2 days before go live
                dAssignStartDate = AssignStartEndDates(-2, rdpStartDate.SelectedDate)
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Move to Captive Portal (" & txtEventName.Text & " WiFi)", dAssignStartDate))

                ' Need to notify NS 2 weeks in advance
                dAssignStartDate = AssignStartEndDates(-14, rdpStartDate.SelectedDate)
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Notify NS (" & txtEventName.Text & " WiFi)", dAssignStartDate))

                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Test Site Tested and Approved (" & txtEventName.Text & " WiFi)", dDummay))
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Test Site Created (" & txtEventName.Text & " WiFi)", dDummay))
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Content Due (" & txtEventName.Text & " WiFi) " & rdpContentDueDate.SelectedDate, rdpContentDueDate.SelectedDate))
                TurnIntoSubTask(sMainTask, CreateSubTask(lclFacility, "Intro Email (" & txtEventName.Text & " WiFi)", dDummay))
            End Using
            Dim tDate As Date
            tDate = dShowStartDate
            AddMoveToCaptivePortalCalendarAppointment(tDate, "Move to Captive Portal " & txtEventName.Text & " - " & txtEventCode.Text)
            tDate = dShowStartDate
            AddNSNotificationCalendarAppointment(tDate, "Notify NS " & txtEventName.Text & " - " & txtEventCode.Text)
            tDate = dShowEndDate
            AddAnalyticsRequestCalendarAppointment(tDate, "Analytics for " & txtEventName.Text & " - " & txtEventCode.Text)
            tDate = rdpContentDueDate.SelectedDate
            AddContentDueCalendarAppointment(tDate, "Analytics for " & txtEventName.Text & " - " & txtEventCode.Text)
            Label1.Text = "Record Created"
            Label1.BackColor = Drawing.Color.LightGreen
        Catch ex As Exception
            ' Error with email.  Mark as error and safe error message
            lblError.Text = ex.Message.ToString
        End Try
        'End If
    End Sub

    Private Function GetReturnedID(ByVal sJSon As String) As String
        sJSon = sJSon.Replace("[", "")
        sJSon = sJSon.Replace("]", "")
        GetReturnedID = ""

        Dim reader As New JsonTextReader(New StringReader(sJSon))
        While reader.Read()
            If reader.Value IsNot Nothing Then
                If reader.Value = "id" Then
                    reader.Read()
                    GetReturnedID = reader.Value
                    Exit While
                End If
            End If
        End While

        Return (GetReturnedID)

    End Function

    Private Sub TurnIntoSubTask(ByVal sMainTaskID As String, ByVal sSubTaskID As String)
        Dim url As String

        ' commenting out if statement because I'm using a PrimanentToken
        'If Not Session("stoken") Is Nothing Then

        ' Create new sub task.  It will get associated with the main task later
        url = "https://www.wrike.com/api/v3/tasks/" & sSubTaskID & "?addSuperTasks=['" & sMainTaskID & "']"
        Try

            Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
            request__1.Headers.Add("Authorization: Bearer " & PrimanentToken)
            request__1.Method = "PUT"
            Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                Dim reader As New StreamReader(response__2.GetResponseStream())

                Dim sResult = reader.ReadToEnd()
                TextBox1.Text = sResult
            End Using

        Catch ex As Exception
            ' Error with email.  Mark as error and safe error message
            lblError.Text = ex.Message.ToString
        End Try
        'End If
    End Sub
    Private Function CreateSubTask(ByVal sMainTaskID As String, ByVal sTitle As String, ByVal dStartDate As Date) As String
        Dim url As String
        CreateSubTask = "0"

        ' commenting out if statement because I'm using a PrimanentToken
        'If Not Session("stoken") Is Nothing Then

        Dim sTempStartDate, sTempEndDate As String

        'Wrike requires date to be yyyy-mm-dd
        sTempStartDate = dStartDate.ToString("yyyy-MM-dd")
        sTempEndDate = dStartDate.ToString("yyyy-MM-dd")


        ' Create new sub task.  It will get associated with the main task later
        If dStartDate = "1/1/1900" Then
            url = "https://www.wrike.com/api/v3/folders/" & sMainTaskID & "/tasks?title=" & sTitle
        Else
            url = "https://www.wrike.com/api/v3/folders/" & sMainTaskID & "/tasks?title=" & sTitle & "&dates={'start':'" & sTempStartDate & "','due':'" & sTempEndDate & "'}"
        End If
            Try

                Dim request__1 As HttpWebRequest = TryCast(WebRequest.Create(url), HttpWebRequest)
                request__1.Headers.Add("Authorization: Bearer " & PrimanentToken)
                request__1.Method = "Post"
                Using response__2 As HttpWebResponse = TryCast(request__1.GetResponse(), HttpWebResponse)
                    Dim reader As New StreamReader(response__2.GetResponseStream())

                    Dim sResult = reader.ReadToEnd()
                    TextBox1.Text = sResult
                    CreateSubTask = GetReturnedID(sResult)
                End Using

            Catch ex As Exception
                ' Error with email.  Mark as error and safe error message
                lblError.Text = ex.Message.ToString
            End Try
        'End If

        Return CreateSubTask
    End Function

    Private Sub LoadFromEMS()
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EMS-Production").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String
            Dim tDate As Date

            ' remember to add the event code later
            sqlString = "SELECT CONVERT(char(10), start_date,126) as eventstart, CONVERT(char(10), end_date,126) as eventend,CONVERT(char(10), start_date,101) as showeventstart, CONVERT(char(10), end_date,101) as showeventend, name  AS name, facility_id AS facility from ems_event WHERE (ems_event.eventCode =  @EventCode)"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()
                        rdpStartDate.DbSelectedDate = XmyReader("showeventstart")
                        rdpEndDate.DbSelectedDate = XmyReader("showeventend")
                        dShowStartDate = XmyReader("showeventstart")
                        dStartDate = XmyReader("eventstart")
                        dShowEndDate = XmyReader("showeventend")
                        dEndDate = XmyReader("eventend")

                        'Wrike requires date to be yyyy-mm-dd
                        tDate = dStartDate
                        dStartDate = tDate.ToString("yyyy-MM-dd")
                        tDate = dEndDate
                        dEndDate = tDate.ToString("yyyy-MM-dd")

                        txtEventName.Text = XmyReader("name")
                        txtFacility.Text = XmyReader("facility")
                        txtFacility.Text = XmyReader("facility")

                    Loop
                End Using
            End Using
            tDate = rdpEndDate.SelectedDate
            rdpContentDueDate.SelectedDate = tDate.AddDays(-30)
            txtSponsorName.Text = "N/A"
            txtSponsorName.Text = "N/A"
            txtEventURL.Text = "N/A"
            chkSSIDTermSheet.Checked = False
            chkPasswordTermSheet.Checked = False
            txtSSID.Text = "N/A"
            txtSSIDPassword.Text = "N/A"
            txtClearCase.Text = "N/A"
            txtContactFirstName.Text = "N/A"
            txtContact.Text = "N/A"
            txtEmail.Text = "N/A"

        End Using

    End Sub
    Private Sub LoadFromEMS_DELETE()

        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EMS-Production").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            ' remember to add the event code later
            sqlString = "SELECT CONVERT(char(10), start_date,126) as eventstart, CONVERT(char(10), end_date,126) as eventend,CONVERT(char(10), start_date,101) as showeventstart, CONVERT(char(10), end_date,101) as showeventend, name  AS name, facility_id AS facility from ems_event WHERE (ems_event.eventCode =  @EventCode)"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()
                        rdpStartDate.DbSelectedDate = XmyReader("showeventstart")
                        rdpEndDate.DbSelectedDate = XmyReader("showeventend")
                        txtEventName.Text = XmyReader("name")
                        txtFacility.Text = XmyReader("facility")
                        'If txtFacility.Text = 1 Then
                        'txtFacility.Text = "BCEC"
                        'Else
                        'txtFacility.Text = "Hynes"
                        'End If
                    Loop
                End Using
            End Using
        End Using

        Dim tDate As Date
        tDate = rdpEndDate.SelectedDate
        rdpContentDueDate.SelectedDate = tDate.AddDays(-30)
        txtSponsorName.Text = "N/A"
        txtEventURL.Text = "N/A"
        chkSSIDTermSheet.Checked = False
        chkPasswordTermSheet.Checked = False
        txtSSID.Text = "N/A"
        txtSSIDPassword.Text = "N/A"
        txtClearCase.Text = "N/A"
        txtContactFirstName.Text = "N/A"
        txtContact.Text = "N/A"
        txtEmail.Text = "N/A"

    End Sub

    Private Function LoadFromLocalDB()
        LoadFromLocalDB = False
        Using myConnection As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SplashPages").ToString)
            myConnection.Open()

            ' figure out which facility to show and which event to display.  More then one event could be going on
            Dim sqlString As String

            ' remember to add the event code later
            sqlString = "SELECT EventName, CONVERT(char(10), StartDate,126) as eventstart, StartDate, CONVERT(char(10), StartDate,101) as showeventstart, CONVERT(char(10), EndDate,126) as eventend, CONVERT(char(10), EndDate,101) as showeventend, EndDate, ContentDueDate, Facility, SponsorsName, ReDirectURL, SSID, SSIDPassword, PrimaryContactFirstName, ContactFullName, ContactEmail, SSIDTermSheet, PasswordTermSheet, ClearCase from SplashPageInfo WHERE eventCode =  @EventCode"

            Using XmyCommand As SqlCommand = New SqlCommand(sqlString, myConnection)
                XmyCommand.Parameters.AddWithValue("@EventCode", txtEventCode.Text)

                Using XmyReader As SqlDataReader = XmyCommand.ExecuteReader()
                    ' loop through events if any
                    Do While XmyReader.Read()


                        dShowStartDate = XmyReader("showeventstart")
                        dStartDate = XmyReader("eventstart")
                        dShowEndDate = XmyReader("showeventend")
                        dEndDate = XmyReader("eventend")

                        LoadFromLocalDB = True
                        txtEventName.Text = XmyReader("EventName")
                        rdpStartDate.DbSelectedDate = XmyReader("StartDate")
                        rdpEndDate.DbSelectedDate = XmyReader("EndDate")
                        rdpContentDueDate.SelectedDate = XmyReader("ContentDueDate")
                        txtFacility.Text = XmyReader("facility")
                        txtSponsorName.Text = XmyReader("SponsorsName")
                        txtEventURL.Text = XmyReader("ReDirectURL")
                        txtSSID.Text = XmyReader("SSID")
                        txtSSIDPassword.Text = XmyReader("SSIDPassword")
                        txtClearCase.Text = XmyReader("ClearCase")
                        txtContactFirstName.Text = XmyReader("PrimaryContactFirstName")
                        txtContact.Text = XmyReader("ContactFullName")
                        txtEmail.Text = XmyReader("ContactEmail")
                        If XmyReader("ssidtermsheet") = 0 Then
                            chkSSIDTermSheet.Checked = False
                        Else
                            chkSSIDTermSheet.Checked = True
                        End If
                        If XmyReader("passwordtermsheet") = 0 Then
                            chkPasswordTermSheet.Checked = False
                        Else
                            chkPasswordTermSheet.Checked = True
                        End If
                    Loop
                End Using
            End Using
        End Using

        Return LoadFromLocalDB
    End Function
    Private Sub LoadEmails()

        Dim tstring As String

        txtWelcome.Content = ""
        txtContentReminder.Content = ""
        txtSSIDReminder.Content = ""
        txtContentReceived.Content = ""
        txtTestSite.Content = ""
        txtNSNotification.Content = ""

        If txtContactFirstName.Text <> "N/A" Then
            tstring = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
            txtContentReminder.Content = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
            txtSSIDReminder.Content = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
            txtContentReceived.Content = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
            txtTestSite.Content = "Hi " & txtContactFirstName.Text & ",<br/><br/>"
        Else
            tstring = "Hi,<br/><br/>"
            txtContentReminder.Content = "Hi,<br/><br/>"
            txtSSIDReminder.Content = "Hi,<br/><br/>"
            txtContentReceived.Content = "Hi,<br/><br/>"
            txtTestSite.Content = "Hi,<br/><br/>"
        End If

        txtNSNotification.Content = ""

        Dim tDate As Date
        tDate = rdpContentDueDate.SelectedDate

        tstring = tstring & "I look forward to working with you and your team to create an engaging wireless network splash page.<br/><br/>" & _
                "I have provided a OneDrive link to the Wireless Network Sponsorship Content Creation Guidelines and the Splash Page Template.&nbsp;&nbsp;<a href='https://signatureboston-my.sharepoint.com/personal/vvieira_signatureboston_com/_layouts/15/guestaccess.aspx?folderid=14b325440b19f49a082300e16a4c213d4&authkey=AfhW2X818E2OZTVlcTkdOiM'>https://signatureboston-my.sharepoint.com/personal/vvieira_signatureboston_com/_layouts/15/guestaccess.aspx?folderid=14b325440b19f49a082300e16a4c213d4&authkey=AfhW2X818E2OZTVlcTkdOiM</a> The guidelines and template are tools that will assist you with creating your splash page.<br/><br/>" & _
                "Please let me know what SSID name and URL re-direct you would like to use for your splash page.&nbsp;&nbsp;The SSID is the public name of a wireless network, e.g., " & txtEventName.Text & ".&nbsp;&nbsp;The URL re-direct is the website the attendee will be directed to after they click 'Continue' on the splash page.&nbsp;&nbsp;It is usually the show's or sponsor's website.<br/><br/>" & _
                "The content submission deadline is " & tDate.ToLongDateString & ".&nbsp;&nbsp;Once the content is received, we will test it and let you know if any adjustments are necessary.<br/><br/>" & _
                "Feel free to contact me if you have any questions.<br/><br/>" & _
                "All the best,<br/>" & _
                "Bob"
        txtWelcome.Content = tstring

        ' Load Content reminder
        txtContentReminder.Content = txtContentReminder.Content & "I hope all is well!&nbsp;&nbsp;I wanted to check in to see if you had any questions regarding the creation of your splash page, and to give you a friendly reminder that the content submission deadline is " & tDate.ToLongDateString & "<br/><br/>" & _
                "Feel free to contact me if you have any questions.<br/><br/>" & _
                "All the best,<br/>Bob"

        Dim dTemp As Date
        dTemp = rdpStartDate.SelectedDate

        If txtFacility.Text = 1 Then
            tstring = "BCEC"
        Else
            tstring = "Hynes"
        End If
        txtNSNotification.Content = txtNSNotification.Content & "Hi Everyone,<br/><br/>" &
            "The details for: " & txtEventName.Text & "s' Wi-Fi splash page are as follows:<br/><br/>" &
            "Start Date:&nbsp;&nbsp;&nbsp;" & dTemp.AddDays(-1).ToShortDateString & "&nbsp;&nbsp;&nbsp;Start Time:	Earliest Convenience<br/>" &
            "(Contracted)&nbsp;&nbsp;&nbsp;" & dTemp.ToShortDateString & "&nbsp;&nbsp;&nbsp;Start Time:	Earliest Convenience<br/>" &
            "End Date:&nbsp;&nbsp;&nbsp;" & rdpEndDate.SelectedDate & "&nbsp;&nbsp;&nbsp;End Time:	12:00pm<br/>" &
            "SSID:&nbsp;&nbsp;&nbsp;" & txtSSID.Text & "<br/>" &
            "SSID Password :&nbsp;&nbsp;&nbsp;" & txtSSIDPassword.Text & "<br/>" &
            "Re-direct URL:&nbsp;&nbsp;&nbsp;" & txtEventURL.Text & "<br/>" &
            "Location:&nbsp;&nbsp;&nbsp;" & tstring & "<br/>" &
            "BCEC Wireless : Enabled<br/>" &
            "Clear Case Profile:&nbsp;&nbsp;&nbsp;" & txtClearCase.Text & "<br/>" &
            "Let me know if you have any questions.<br/><br/>" &
            "Thanks,<br/>" &
            "Bob"

        txtContentReceived.Content = txtContentReceived.Content & "Thank you for sending the Wi-Fi splash page files.&nbsp;&nbsp;We will review them and let you know if any adjustments are necessary.<br/><br/>" & _
            "All the best,<br/>Bob"

        dTemp = rdpContentDueDate.SelectedDate
        txtSSIDReminder.Content = txtSSIDReminder.Content & "I hope all is well!&nbsp;&nbsp;Please let me know what SSID name and URL re-direct you would like to use for your splash page.&nbsp;&nbsp;The SSID is the public name of a wireless network, e.g., " & txtEventName.Text & ".&nbsp;&nbsp;The URL re-direct is the website the attendee will be directed to after they click 'Continue' on the splash page.&nbsp;&nbsp;It is usually the show's or sponsor's website.<br/><br/>" & _
                    "Also, a friendly reminder that the content submission deadline is " & tDate.ToLongDateString & "<br/><br/>" & _
                    "Feel free to contact me if you have any questions.<br/><br/>" & _
                    "All the best,<br/>Bob"

        txtTestSite.Content = txtTestSite.Content & "The following link is to your splash page test site, <a href='http://64.251.112.13/splashpage/" & txtEventName.Text & "/index.html'>http://64.251.112.13/splashpage/" & txtEventName.Text & "/index.html</a>.&nbsp;&nbsp;Please take a look and let me know if you have any questions.<br/><br/>" & _
                "All the best,<br/>Bob"

        dTemp = rdpStartDate.SelectedDate
        txtSendAnalyticsRequest.Content = "Hi All,<br/><br/>" & _
            "Can I please get the analytics for:<br/>" & _
            "SSID:&nbsp;&nbsp;&nbsp;" & txtSSID.Text & "<br/>" & _
            "Location:&nbsp;&nbsp;&nbsp;" & tstring & "<br/>" & _
            "Start Date:&nbsp;&nbsp;&nbsp;" & dTemp.ToShortDateString & "<br/>" & _
            "End Date:&nbsp;&nbsp;&nbsp;" & rdpEndDate.SelectedDate & "<br/><br/>" & _
            "Thank you,<br/>Bob"

        txtTestEmail.Content = "Hi All,<br/><br/>" & _
            "Test<br/>" & _
            "Test<br/>" & _
            "Test<br/>" & _
            "Test<br/>" & _
            "Test<br/><br/>" & _
            "Thank you,<br/>Bob"
    End Sub
    Protected Sub btnLoadEvent_Click(sender As Object, e As EventArgs)

        ' completely load event from EMS
        'GetEventInfo()
        Session("sLoadedFromLocalDB") = True

        If LoadFromLocalDB() = False Then
            Session("sLoadedFromLocalDB") = False
            LoadFromEMS()
        End If

        LoadEmails()



    End Sub

    Protected Sub AddContentDueCalendarAppointment(ByVal sDueDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = "Content is due: " & txtEventName.Text & " on " & sDueDate & " ID: " & txtEventCode.Text
        TempAppItem.Body = "Send content due EMail"

        'Set Location
        TempAppItem.Location = "No Location"
        Dim dAssignStartDate As Date

        ' Content due date is the correct date.  No need to subtract or add any days.  Function will make sure date doesn't land on a Sat or Sun
        dAssignStartDate = AssignStartEndDates(0, sDueDate)

        Dim sTempStartDate, sTempEndDate As String
        sTempStartDate = dAssignStartDate.ToShortDateString() & " 7:00:00 AM"
        sTempEndDate = dAssignStartDate.ToShortDateString() & " 8:00:00 AM"

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub
    Protected Sub AddAnalyticsRequestCalendarAppointment(ByVal sEndDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = "Please send analytics for SSID: " & txtSSID.Text & " from " & dShowStartDate & " to " & dShowEndDate
        'Set Location
        TempAppItem.Location = "No Location"

        Dim dAssignStartDate As Date

        ' Ask for the analytics one date after the end date.  Function will make sure date doesn't land on a Sat or Sun
        dAssignStartDate = AssignStartEndDates(1, sEndDate)

        Dim sTempStartDate, sTempEndDate As String
        sTempStartDate = dAssignStartDate.ToShortDateString() & " 7:00:00 AM"
        sTempEndDate = dAssignStartDate.ToShortDateString() & " 8:00:00 AM"

        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub

    Protected Sub AddNSNotificationCalendarAppointment(ByVal sStartDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = sSubject
        'Set Location
        TempAppItem.Location = "No Location"

        Dim dAssignStartDate As Date
        Dim sTempStartDate, sTempEndDate As String

        ' Need to notify NS 2 weeks in advance
        dAssignStartDate = AssignStartEndDates(-14, sStartDate)
        sTempStartDate = dAssignStartDate.ToShortDateString() & " 7:00:00 AM"
        sTempEndDate = dAssignStartDate.ToShortDateString() & " 8:00:00 AM"


        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub
    Protected Sub AddMoveToCaptivePortalCalendarAppointment(ByVal sStartDate As Date, ByVal sSubject As String)
        Dim TempApp As Outlook.Application = New Outlook.Application()

        'An AppointmentItem 'TempAppItem' object represent one appointment              
        Dim TempAppItem As Outlook.AppointmentItem = TempApp.CreateItem(Outlook.OlItemType.olAppointmentItem)

        TempAppItem.Subject = sSubject
        TempAppItem.Body = sSubject
        'Set Location
        TempAppItem.Location = "No Location"

        Dim dAssignStartDate As Date
        Dim sTempStartDate, sTempEndDate As String

        ' Move code to captive portal 2 days before go live
        dAssignStartDate = AssignStartEndDates(-2, sStartDate)
        sTempStartDate = dAssignStartDate.ToShortDateString() & " 7:00:00 AM"
        sTempEndDate = dAssignStartDate.ToShortDateString() & " 8:00:00 AM"


        'Set start  date and times
        TempAppItem.Start = Convert.ToDateTime(sTempStartDate)
        'Set  end date and times
        TempAppItem.End = Convert.ToDateTime(sTempEndDate)

        'set the Reminder box if you want Outlook to remind you the appointment is coming up.
        TempAppItem.ReminderSet = True
        TempAppItem.ReminderMinutesBeforeStart = 30

        'we can choose how the time is categorized (Busy, Tentative, Free, and so on)
        TempAppItem.BusyStatus = Outlook.OlBusyStatus.olFree

        TempAppItem.IsOnlineMeeting = False

        ' Save to Calendar.
        TempAppItem.Save()

        TempApp = Nothing
        TempAppItem = Nothing
    End Sub
    Protected Function AssignStartEndDates(ByVal iNbrOfDays As Integer, ByVal stDate As Date) As Date
        If iNbrOfDays < 0 Then
            ' if calculated date lands on a Sunday push back to Friday
            If stDate.AddDays(iNbrOfDays).DayOfWeek = DayOfWeek.Sunday Then
                AssignStartEndDates = stDate.AddDays(iNbrOfDays - 2)
                ' if calculated date lands on a Saturday push back to Friday
            ElseIf stDate.AddDays(iNbrOfDays).DayOfWeek = DayOfWeek.Saturday Then
                AssignStartEndDates = stDate.AddDays(iNbrOfDays - 1)

            Else ' else just back up number of days in iNbrOfDays
                AssignStartEndDates = stDate.AddDays(iNbrOfDays)

            End If
        Else
            ' if calculated date lands on a Sunday push forward to Monday
            If stDate.AddDays(iNbrOfDays).DayOfWeek = DayOfWeek.Sunday Then
                AssignStartEndDates = stDate.AddDays(iNbrOfDays + 1)
                ' if calculated date lands on a Saturday forward to Monday
            ElseIf stDate.AddDays(iNbrOfDays).DayOfWeek = DayOfWeek.Saturday Then
                AssignStartEndDates = stDate.AddDays(iNbrOfDays + 2)

            Else ' else just back up number of days in iNbrOfDays
                AssignStartEndDates = stDate.AddDays(iNbrOfDays)

            End If
        End If

        Return AssignStartEndDates
    End Function

    Protected Sub btnSaveEvent_Click(sender As Object, e As EventArgs)
        SaveEvent()
        LoadEmails()
        Session("sLoadedFromLocalDB") = True

    End Sub

    Protected Sub btnSendWelcome_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtWelcome, txtEmail.Text)
    End Sub

    Protected Sub btnSendContentReceived_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtContentReceived, txtEmail.Text)

    End Sub

    Protected Sub btnSendTestSite_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtTestSite, txtEmail.Text)

    End Sub

    Protected Sub btnSendContentReminder_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtContentReminder, txtEmail.Text)

    End Sub

    Protected Sub btnSendSSIDReminder_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtSSIDReminder, txtEmail.Text)

    End Sub

    Protected Sub btnSendNSNotification_Click(sender As Object, e As EventArgs)
        SendIndividualEmails(txtEventName.Text & " - Wi-Fi Sponsorship", txtNSNotification, "MCCANS@signatureboston.com")

    End Sub

    Protected Sub btnSendAnalyticsRequest_Click(sender As Object, e As EventArgs)
        SendIndividualEmails("Splash page analytics", txtSendAnalyticsRequest, "MCCANS@signatureboston.com")
    End Sub
    Protected Sub btnSendTest_Click(sender As Object, e As EventArgs)
        SendIndividualEmails("Splash page test email", txtTestEmail, "rplatz@signatureboston.com")
    End Sub
    Sub SendIndividualEmails(ByVal sEmailSubject As String, ByVal txtConent As Object, ByVal sEmail As String)
        Try

            '(1) Create the MailMessage instance
            Dim mm As New MailMessage()

            ' set the 'From:' to have them send replies to me
            mm.From = New MailAddress("rplatz@massconvention.com", "Bob Platz")


            ' Set the 'To:'  (will use the Web.config settings)
            Dim strMailingMode As String
            strMailingMode = System.Configuration.ConfigurationManager.AppSettings("SplashPageEmailing")

            If strMailingMode <> Nothing Then
                If (strMailingMode.ToUpper = "DEBUG") Then

                    mm.To.Add(New MailAddress("rplatz@massconvention.com", "Bob Platz"))
                Else
                    mm.To.Add(New MailAddress("rplatz@massconvention.com", "Bob Platz"))
                    mm.To.Add(New MailAddress("jflaherty@bostoncmc.com", "Jessica Flaherty"))
                    mm.To.Add(New MailAddress("tburgio@signatureboston.com", "Tracy Burgio"))
                    mm.To.Add(New MailAddress(sEmail, sEmail))
                End If
            Else
                mm.To.Add(New MailAddress("rplatz@massconvention.com", "Bob Platz"))
                mm.To.Add(New MailAddress(sEmail, sEmail))
            End If

            'mm.Bcc.Add(New MailAddress("rplatz@massconvention.com", "Robert Platz"))
            'mm.Bcc.Add(New MailAddress("smcdermott@massconvention.com ", "Shannon McDermott"))


            '(2) Assign the MailMessage's properties
            mm.Subject = sEmailSubject
            mm.Body = txtConent.Content
            mm.IsBodyHtml = True

            '(3) Create the SmtpClient object
            Dim smtp As New SmtpClient

            '(4) Send the MailMessage
            ' send to everyone
            smtp.Send(mm)

            'System.Threading.Thread.Sleep(2000)


        Catch ex As Exception
            ' Error with email.  Mark as error and safe error message
            lblError.Text = ex.Message.ToString
        End Try

    End Sub

End Class